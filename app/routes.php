<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	return View::make('hello');
});
Route::get('book/newbook',array('as' => 'form.book', function()
{
	return View::make('newbook');
}));
//insertbook
Route::post('book/insert', 'BookController@store');
//listbook
Route::get('book/list',['as' => 'list.book', 'uses' => 'BookController@index']);
//edit
Route::get('book/edit/{id}',['as' => 'edit.book', 'uses' => 'BookController@edit']);
//update
Route::post('book/update',['as' => 'update.book', 'uses' => 'BookController@update']);
//delete
Route::get('book/delete/{id}',['as' => 'delete.book', 'uses' => 'BookController@delete']);