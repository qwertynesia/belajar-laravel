<?php
class BookController extends BaseController
{
	protected $book;
	function __construct(Book $book) {
		$this->book = $book;
	}
	//listbook
	public function index()
	{
		$books = $this->book->all();
		// $books = Book::all();
		return View::make('listbook')->with('booksdata',$books);

	}
	//implement save data dengan instance object biasa
	public function store()
	{
		$book = new Book;
		$book->title       = Input::get('title');
	    $book->author      = Input::get('author');
	    $book->description = Input::get('description');
	    $book->price       = Input::get('price');
	    //save
	    $book->save();
	    return Redirect::route('form.book')->with('message', 'Berhasil Input');
	}
	//implement edit
	public function edit($id)
	{
		$book = $this->book->find($id);
		return View::make('editbook')->with('book',$book);
	}
	
	//implement update
	public function update()
	{
		$id   = Input::get('id');
      	// $book = Book::find($id);
      	$book = $this->book->find($id);

      	$book->title       = Input::get('title');
     	$book->author      = Input::get('author');
     	$book->description = Input::get('description');
     	$book->price       = Input::get('price');

     	$book->save();
     	return Redirect::route('list.book')->with('message', 'Book Updated!!');
	}
	//implement delete with ioc
	public function delete($id)
	{
		$val = $this->book->find($id)->delete();
		return Redirect::route('list.book')->with('message', 'Book Deleted!');
	}
}