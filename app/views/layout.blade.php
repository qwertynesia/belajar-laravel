<!DOCTYPE>
<html lang='en'>
	<head>
		<meta charset='utf-8'>
		<title>Crud App</title>
		<meta name='viewport' content='width=device-width, initial-scale=1, maximum-scale=1' />
		{{HTML::style('assets/css/bootstrap.min.css')}}
	</head>
	<body>
		@yield('content')
		{{HTML::script('assets/js/jquery-migrate-1.2.1.min.js')}}
		{{HTML::script('assets/js/bootstrap.min.js')}}
	</body>
</html>