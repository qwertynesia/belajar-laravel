@extends('layout')
	@section('content')
		<section class="container">
			@if (Session::has('message'))
				<div class="alert alert-success">
					<center>{{Session::get('message')}}</center>
				</div>
			@endif
		 <table class="table">
		    <tr>
		       <th>Title</th>
		       <th>Author</th>
		       <th>Description</th>
		       <th>Price</th>
		       <th>Action</th>
		    </tr>
		    @foreach($booksdata as $book)
		      <tr>
		         <td>{{ $book->title }}</td>
		         <td>{{ $book->author }}</td>
		         <td>{{ $book->description }}</td>
		         <td>{{ $book->price }}</td>
		         <td>{{HTML::linkAction('edit.book','Edit',array('id' => $book->id))}} | {{HTML::linkAction('delete.book','Delete',array('id' => $book->id))}}</td>
		      </tr>
		    @endforeach
		 </table>
		</section>
	@stop